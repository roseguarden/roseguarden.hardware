# Olimex EVB

ESP32 based hardware for roseguarden.
The setup based on the available modules from olimex only.
Including the ESP32-EVB and UEXT-Modules (Display and RFID Reader).


## BOM

ESP32 Evaluation Board - https://www.olimex.com/Products/IoT/ESP32/ESP32-EVB

* Relay and SD Card used
* **HINT:** Please consider to get ESP32-EVB-EA if you want to use the housing from Olimex

ESP32 EVB housing - https://www.olimex.com/Products/IoT/ESP32/BOX-ESP32-EVB-EA/
 
* Optional
* Housing
* **HINT:** The ESP32-EVB-EA have to be used and is not included.

RFID Reader (UEXT) - https://www.olimex.com/Products/Modules/RFID/MOD-RFID1356MIFARE/

* 13,56 MHz RFID reader
* Useable with almost all rfid tags smartphones or ids
* Communication over UART
* Power Supply 3,3V (a jumper have to be soldered to allow supply over the uext)

Display with touch (UEXT) - https://www.olimex.com/Products/Modules/LCD/MOD-LCD2-8RTP

* 2,8 Inch Display
* 320x240 Pixel
* RTP Touch
* SPI Interface
* 3,3V Power suply

