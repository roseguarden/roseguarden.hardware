/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "esp_vfs.h"
#include "esp_spiffs.h"
#include "esp_heap_caps.h"
#include "ili9340.h"
#include "ili9340_test.h"

#define WAIT vTaskDelay(INTERVAL)

#define CONFIG_WIDTH 240
#define CONFIG_HEIGHT 320
#define CONFIG_MISO_GPIO 15
#define CONFIG_MOSI_GPIO 2
#define CONFIG_SCLK_GPIO 14
#define CONFIG_CS_GPIO 5

#define CONFIG_DC_GPIO 15
#define CONFIG_RESET_GPIO -1
#define CONFIG_BL_GPIO -1
#define CONFIG_OFFSETX 0
#define CONFIG_OFFSETY 0

static const char *TAG = "ILI9340";

static void SPIFFS_Directory(char *path)
{
    DIR *dir = opendir(path);
    assert(dir != NULL);
    while (true)
    {
        struct dirent *pe = readdir(dir);
        if (!pe)
            break;
        ESP_LOGI(__FUNCTION__, "d_name=%s d_ino=%d d_type=%x", pe->d_name, pe->d_ino, pe->d_type);
    }
    closedir(dir);
}

void run_ILI9341_test(void *pvParameters)
{
    FontxFile fx24G[2];
    char file[32];
    InitFontx(fx24G, "/spiffs/ILGH24XB.FNT", ""); // 12x24Dot Gothic
    uint8_t text_buffer[40];
    uint16_t text_color = WHITE;

    TFT_t dev;
    spi_master_init(&dev, CONFIG_MOSI_GPIO, CONFIG_SCLK_GPIO, CONFIG_CS_GPIO, CONFIG_DC_GPIO, CONFIG_RESET_GPIO, CONFIG_BL_GPIO);
    uint16_t model = 0x9341; // ILI9341
    lcdInit(&dev, model, CONFIG_WIDTH, CONFIG_HEIGHT, CONFIG_OFFSETX, CONFIG_OFFSETY);

    while (1)
    {
        FillTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
        strcpy((char *)text_buffer, "16Dot Gothic Font");
        lcdSetFontDirection(&dev, 1);
        lcdDrawString(&dev, fx24G, 10, 10, text_buffer, text_color);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        strcpy(file, "/spiffs/esp_logo.png");
        PNGTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        printf("Tick\n");
    }
}

void app_main()
{
    printf("Startup\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
           chip_info.cores,
           (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
           (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
        .base_path = "/spiffs",
        .partition_label = NULL,
        .max_files = 10,
        .format_if_mount_failed = true};

    // Use settings defined above toinitialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is anall-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK)
    {
        if (ret == ESP_FAIL)
        {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        }
        else if (ret == ESP_ERR_NOT_FOUND)
        {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        }
        else
        {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    }
    else
    {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }

    SPIFFS_Directory("/spiffs/");
    xTaskCreate(run_ILI9341_test, "ILI9341", 1024 * 6, NULL, 2, NULL);
}
